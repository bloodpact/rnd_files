const express = require("express");
const router = express.Router();
const testFolder = "I:/d/ФОТО";
const { getCurrentDirfiles, getFiles, rndFiles, copyFiles } = require("./files");
router.post("/", (req, res) => {
    console.log(req);
});
router.get("/rnd", (req, res) => { 
    const choosenFiles = rndFiles(getFiles(testFolder));
    copyFiles(choosenFiles);
    res.send(choosenFiles);
    console.log('request', choosenFiles);
    
});


router.get("/list", (req, res) => { 
    const dirs = getCurrentDirfiles(testFolder);
    const choosenFiles = dirs.map((e) => rndFiles(getFiles(`I:/d/ФОТО/${e}`))).flat();
    copyFiles(choosenFiles);
    res.send(choosenFiles);
});

module.exports = router;
