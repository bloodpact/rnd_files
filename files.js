const fs = require("fs");
const path = require('path');
const { isArray } = require("util");
const size = 2;

const getFiles = (dir, dirFiles) => {
    dirFiles = dirFiles || [];
    let files = fs.readdirSync(dir);
    for (let i in files) {
        let name = dir + "/" + files[i];
        if (fs.statSync(name).isDirectory()) {
            getFiles(name, dirFiles);
        } else {
            dirFiles.push(name);
        }
    }
    return dirFiles;
};
const getCurrentDirfiles = (dir) => {
    // return fs
    //     .readdirSync(dir, { withFileTypes: true })
    //     .filter((item) => !item.isDirectory())
    //     .map((item) => item.name);
    const dirs =  fs
                   .readdirSync(dir, { withFileTypes: true })
                   .filter((item) => item.isDirectory())
                   .map((item) => item.name)
    return dirs;
};

const rndFiles = (files) => {
    // const randoms = Array.from({length: size}, () => Math.floor(Math.random() * files.length));
    let _arr = [...files];
    const choosenFiles = [...Array(size)].map( ()=> _arr.splice(Math.floor(Math.random() * _arr.length), 1)[0] ); 
    return choosenFiles;

};

const copyFiles = (files) =>{
    const fileNames = files.map((e) => {
        return e.split('/')[e.split('/').length -1]
    })
    files.forEach(async file => {
        await fs.copyFile(path.join(file), path.join('H:/projects/rnd-files-front/public/album/' + path.basename(file).toLowerCase()), err => {
          if(!err){
          console.log(file + " has been copied!");
            }
        })
      
      })
    
}



module.exports = { getCurrentDirfiles, getFiles, rndFiles, copyFiles };
