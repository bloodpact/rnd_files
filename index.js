const express = require("express");
const router = require("./routes");
const cors = require('cors');
const app = express();

app.use(cors());
app.use("/", router);
const PORT = 3001 || process.env.PORT;

app.listen(PORT, async () => {
    try {
        console.log(`server started at ${PORT}`);
    } catch (e) {
        console.log(e);
    }
});
